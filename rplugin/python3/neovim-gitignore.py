import os
import os.path
import re
import fnmatch
import neovim


@neovim.plugin
class GitIgnore:
    def __init__(self, vim):
        self.vim = vim

    def _normalize(self, data):
        regex = re.compile(r'(?:^\s*$)|(?:^\s*#|!.*$)')
        return filter(lambda x: not regex.match(x), data)

    def _translate_to_re_list(self, path):
        with open(path) as f:
            patterns = f.read()

        res = []
        for pattern in self._normalize(patterns.splitlines()):
            regex = fnmatch.translate(pattern)
            res.append(regex)

        return res

    def _parse_ignore(self, path):
        with open(path) as f:
            patterns = f.read()

        for pattern in patterns.splitlines():
            pattern = pattern.strip()

            if pattern.startswith('#'):
                continue
            elif pattern.startswith('!'):
                self.vim.out_write('WARNING: "!" are not supported!\n')
                continue
            elif pattern == '':
                continue
            elif pattern.endswith('/'):
                pattern += '*'

            pattern = pattern.replace(',', '\\\\,')

            yield pattern

    def add_wig_pattern(self, pattern):
        self.vim.command('set wildignore+={}'.format(pattern))

    def remove_wig_pattern(self, pattern):
        self.vim.command('set wildignore-={}'.format(pattern))

    def set_nerdignore(self, regexes):
        cmd = 'let g:NERDTreeIgnore={}'.format(str(regexes))
        cmd = cmd.replace('\\\\', '\\').replace(r'\Z(?ms)', '')
        self.vim.command(cmd)

    # TODO: Fix code duplication
    @neovim.command('GitIgnAdd', complete='file', nargs='+', sync=True)
    def add_file(self, args):
        for arg in args:
            path = os.path.expanduser(arg)
            if os.path.exists(path):
                for pattern in self._parse_ignore(path):
                    self.add_wig_pattern(pattern)

    @neovim.command('GitIgnDel', complete='file', nargs='+', sync=True)
    def remove_file(self, args):
        for arg in args:
            path = os.path.expanduser(arg)
            if os.path.exists(path):
                for pattern in self._parse_ignore(path):
                    self.remove_wig_pattern(pattern)

    @neovim.command('GitIgnAddNERDTree', complete='file', nargs='+', sync=True)
    def add_file_to_nerd_tree(self, args):
        for arg in args:
            path = os.path.expanduser(arg)
            if os.path.exists(path):
                self.set_nerdignore(self._translate_to_re_list(path))
